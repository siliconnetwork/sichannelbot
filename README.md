## SiChannelBot

This is a bot to manage the channels @siliconnetwork operates on Telegram easier

```
git clone {url}
pyvenv env
source env/bin/activate
pip install -r requirements.txt

# edit config.json.example and save to config.json

python main.py
```
