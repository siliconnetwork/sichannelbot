"""
SiChannelBot, the bot that manages the telgram channels of
the @siliconnetwork

Copyright © 2017 Adrian Tschira

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
import asyncio
import signal
import json

import telepot.aio
from telepot.exception import TelegramError
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton

import aiocron

import motor.motor_asyncio

with open("config.json") as f:
    CONFIG = json.load(f)

print("TOKEN: ", CONFIG["token"])

#for testing in cli
def run_sync(future):
    return asyncio.get_event_loop().run_until_complete(future)


class UserManager:
    def __init__(self, users):
        self._users = users

    async def add_user(self, userid, authorization):
        """
        add an administrator to the user list
        """
        raise NotImplementedError()

    async def get_authorization(self, userid):
        """
        gets the authorization level of a user
        """
        try:
            return self._users[userid]["auth"]
        except KeyError:
            return None

    async def is_superuser(self, userid):
        return await self.get_authorization(userid) == "superuser"

    async def is_admin(self, userid):
        return await self.get_authorization(userid) in ("admin", "superuser")

class SiChannelBot(telepot.aio.Bot):
    def __init__(self, db, usermanager, *args, **kwargs):
        self.me = None
        self._db = db
        self._users = usermanager
        super(SiChannelBot, self).__init__(*args, **kwargs)
        async def startup():
            self.me = await self.getMe()
            print("running as", self.me["username"])
        asyncio.ensure_future(startup())

        @aiocron.crontab("0 12 * * *")
        async def post_cron():
            await self.cron_post_action()

    async def on_chat_message(self, message):
        """
        called when a telegram message arrives
        """
        print("message recieved:", message)
        if not await self._users.is_admin(message["from"]["id"]):
            await self.sendMessage(message["from"]["id"], "unauthorized")
            return

        if message["text"].startswith("/addchannel"):
            await self.new_channel_action(message)

        elif message["text"].startswith("/list"):
            await self.list_channels_action(message)

        elif message["text"].startswith("/queue"):
            await self.list_queue_action(message)

        elif message["text"].startswith("/post"):
            await self.cron_post_action()

        else:
            await self.queue_menu_action(message)

        print("done")

    async def list_queue_action(self, message):
        content_type, chat_type, chat_id = telepot.glance(message)

    async def on_callback_query(self, message):
        """
        called when an callback query arrives from pressing an inline button
        """
        if not await self._users.is_admin(message["from"]["id"]):
            await self.sendMessage(message["from"]["id"], "unauthorized")
            return

        print("callback:", message)
        args = message["data"].split(" ")
        if args[0] == "queue":
            await self.queue_callback_action(args, message)

        await self.answerCallbackQuery(message["id"])

    async def on_inline_query(self, message):
        """
        stub
        """

    async def on_edited_chat_message(self, message):
        pass

    async def new_post_action(self, message):
        key = {"name": ""}
        # await db.channels.update()

    async def list_channels_action(self, message):
        content_type, chat_type, chat_id = telepot.glance(message)
        channel_titles = [channel["title"] for channel in await self.get_channel_list()]
        await self.sendMessage(chat_id, "\n".join(channel_titles))

    async def get_channel_list(self):
        return await self._db.channels.find().to_list(100)

    async def queue_menu_action(self, message):
        content_type, chat_type, chat_id = telepot.glance(message)

        inline_keyboard = []
        for i in await self.get_channel_list():
            callback_data = "queue {}".format(i["id"])
            inline_keyboard.append([InlineKeyboardButton(text=i["title"],
                callback_data=callback_data)])

        markup = InlineKeyboardMarkup(inline_keyboard=inline_keyboard)
        await self.sendMessage(chat_id, message["text"], reply_markup=markup)

    async def queue_callback_action(self, args, message):
        await self.queue_new_post(message["message"]["text"], int(args[1]), message["from"])

    async def queue_new_post(self, post, channel, posted_by, urgent=False):
        data = {
            "post": post,
            "channel": channel,
            "urgent": urgent,
            "posted_by": posted_by,
        }
        print("inserted data:", data)
        await self._db.posts.insert(data, {"$currentDate": {"created": True}})

    async def new_channel_action(self, message):
        content_type, chat_type, chat_id = telepot.glance(message)
        print(message)

        if "forward_from_chat" in message and message[
                "forward_from_chat"]["type"] == "channel":
            chat = message["forward_from_chat"]
        else:
            text = message["text"].strip()
            try:
                chat = await self.getChat(message["text"].strip())
            except TelegramError as e:
                if e.error_code == 400:
                    await self.sendMessage(chat_id, "There is no channel with that username")
                    return
                raise

        if not chat["type"] == "channel":
            await self.sendMessage(chat_id, "Identifier is not a channel")
            return

        try:
           admins = await self.getChatAdministrators(chat["id"])
        except TelegramError as e:
            if e.error_code == 403:
                await self.sendMessage(chat_id, "This bot is not in the channel")
                return
            raise

        if not any([chat_id == admin["user"]["id"] for admin in admins]):
            await self.sendMessage(chat_id, "you are not admin of the channel")

        added = await self.upsert_channel(chat["id"], chat["title"])
        if added:
            await self.sendMessage(chat_id,
                    "added channel \"{}\"".format(chat["title"]))
        else:
            await self.sendMessage(chat_id,
                    "channel \"{}\" already exists".format(chat["title"]))

    async def upsert_channel(self, channel_id, title):
        print("adding channel", channel_id, title)
        key = {"id": channel_id}
        data = {"id": channel_id, "title": title}
        r = await self._db.channels.update_one(key, {"$set": data}, upsert=True)

        return r.matched_count == 0 # return True if a new channel was added

    async def cron_post_action(self):
        for i in await self.get_channel_list():
            await self.post_to_channel(i)

    async def post_to_channel(self, channel):
        print("posting...")
        post = await self._db.posts.find_one_and_delete({"channel": channel["id"]})

        if post is not None:
            await self.sendMessage(channel["id"], post["post"])
        else:
            print("nothing to post for", channel["title"])

def shutdown(loop):
    for task in asyncio.Task.all_tasks():
        task.cancel()
    loop.stop()

def main():
    client = motor.motor_asyncio.AsyncIOMotorClient()
    db = client.SiChannelBot
    user_manager = UserManager(
        {int(key):value for key, value in CONFIG["users"].items()}
    )
    bot = SiChannelBot(db, user_manager, CONFIG["token"])
    message_loop = bot.message_loop()

    loop = asyncio.get_event_loop()
    loop.add_signal_handler(signal.SIGINT, shutdown, loop)
    loop.create_task(message_loop)
    loop.run_forever()

if __name__ == "__main__":
    main()
